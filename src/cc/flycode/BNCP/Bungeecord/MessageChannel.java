package cc.flycode.BNCP.Bungeecord;

import com.mysql.fabric.Server;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.io.*;
import java.util.Map;

/**
 * Created by FlyCode on 14/08/2018 Package cc.flycode.BNCP.Bungeecord
 */
public class MessageChannel implements Listener {
    @EventHandler
    public void onPluginMessage(PluginMessageEvent e) {
        if (e.getTag().equalsIgnoreCase("BNCPAlerts")) {
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(e.getData()));
            try {
                String channel = in.readUTF();
                Map<String, ServerInfo> servers = BungeeCord.getInstance().getServers();
                for (Map.Entry<String, ServerInfo> en : servers.entrySet()) {
                    String name = en.getKey();
                    ServerInfo all = BungeeCord.getInstance().getServerInfo(name);
                    sendToBukkit(channel.toString(),all);
                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
        private void sendToBukkit(String message, ServerInfo server) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            DataOutputStream out = new DataOutputStream(stream);
            try {
                out.writeUTF(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
            server.sendData("BNCPListener", stream.toByteArray());
        }
}
