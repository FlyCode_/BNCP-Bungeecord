package cc.flycode.BNCP.Bungeecord;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;

/**
 * Created by FlyCode on 14/08/2018 Package cc.flycode.BNCP.Bungeecord
 */
public class BNCPBungeecord extends Plugin {
    @Override
    public void onEnable() {
        getLogger().info("Registering Alerts Channel...");
        BungeeCord.getInstance().registerChannel("BNCPAlerts");
        BungeeCord.getInstance().registerChannel("BNCPListener");
        ProxyServer.getInstance().getPluginManager().registerListener(this,new MessageChannel());
    }
}
